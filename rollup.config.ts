import { RollupOptions } from "rollup"
import typescript from "@rollup/plugin-typescript"
import { generateDtsBundle } from "./src"
import pkg from "./package.json"

const config: RollupOptions = {
  input: "src/index.ts",
  output: {
    file: "lib/index.js",
    format: "cjs",
  },
  external: ["fs", "path", ...Object.keys(pkg.dependencies)],
  plugins: [typescript(), generateDtsBundle()],
}

export default config
