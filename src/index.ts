import { writeFileSync, mkdirSync } from "fs"
import { CompilationOptions, EntryPointConfig, generateDtsBundle as generate } from "dts-bundle-generator"
import { Plugin } from "rollup"
import { basename, dirname, extname, join } from "path"

export type PluginConfig = {
  entry?: EntryPointConfig[] // see https://github.com/timocov/dts-bundle-generator/blob/master/src/bundle-generator.ts
  compilation?: CompilationOptions // see https://github.com/timocov/dts-bundle-generator/blob/master/src/bundle-generator.ts
  outFile?: string // custom output filename, e.g. "types/index.d.ts"
}

export const generateDtsBundle = (pluginConfig: PluginConfig = {}): Plugin => ({
  name: "dts-bundle-generator",
  renderStart(outputConfig, inputConfig) {
    const rollupEntryFile = Array.isArray(inputConfig.input) ? inputConfig.input[0] : inputConfig.input[Object.keys(inputConfig.input)[0]]
    const rollupOutputFile = outputConfig.file
    const rollupOutputDir = outputConfig.dir

    const entry =
      pluginConfig.entry ||
      (rollupEntryFile && [
        {
          filePath: rollupEntryFile,
        },
      ])

    const output =
      pluginConfig.outFile ||
      (rollupOutputFile && join(dirname(rollupOutputFile), basename(rollupOutputFile, extname(rollupOutputFile)) + ".d.ts")) ||
      (rollupOutputDir && join(rollupOutputDir, "index.d.ts"))

    if (!entry) {
      console.log("No entry file specified; skipping .d.ts generation")
      return
    } else if (!output) {
      console.log("No output file specified; skipping .d.ts generation")
      return
    }

    const outDir = dirname(output)

    if (outDir) {
      mkdirSync(outDir, {
        recursive: true,
      })
    }

    const { compilation } = pluginConfig

    console.log(`Generating .d.ts bundle "${output}"`)
    const dts = generate(entry, compilation).join("")
    writeFileSync(output, dts)
  },
})
