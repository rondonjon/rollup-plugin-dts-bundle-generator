# rollup-plugin-dts-bundle-generator

Just a small plugin that wraps the
[dts-bundle-generator](https://www.npmjs.com/package/dts-bundle-generator)
CLI application to ease the integration in
[rollup](https://www.npmjs.com/package/rollup) builds.

## `dts-bundle-generator`

All the heavy lifting in this plugin is done by the `dts-bundle-generator`
CLI application.

`dts-bundle-generator` was created as zero-config solution to generate
a single, flattened `.d.ts` file that contains no further declarations
than those that are exported by the specified entry file and its
descendants.

**I am NOT affiliated with or a maintainer of `dts-bundle-generator`.**

If you're using this plugin and find it helpful, please consider a
[donation](https://opencollective.com/dts-bundle-generator/contribute)
to the authors of the `dts-bundle-generator` CLI application to sustain
their efforts.

## Usage

Installation:

```
npm i -D rollup-plugin-dts-bundle-generator
```

In your rollup configuration:

```
const config = {
  ...
  plugins: [..., generateDtsBundle(options)],
  ...
}
```

If the `options` are omitted / `undefined`, the plugin attempts to derive
reasonable defaults from the input / output values of the rollup
configuration.

The following `options` are currently supported:

```
export type PluginConfig = {
  entry?: EntryPointConfig[]         // see https://github.com/timocov/dts-bundle-generator/blob/master/src/bundle-generator.ts
  compilation?: CompilationOptions   // see https://github.com/timocov/dts-bundle-generator/blob/master/src/bundle-generator.ts
  outFile?: string                   // custom output filename, e.g. "types/index.d.ts"
}
```

## Motivation

A simple, bundled export of TypeScript typings in rollup builds has been among
the most requested features for years and is still difficult to accomplish
without plugins, out of which many seem quirky and difficult to use.

Discussion:

- [Generating d.ts files in TypeScript projects](https://github.com/rollup/plugins/issues/394)
- [Tools comparisson (dts-bundle-generator, api-extractor, rollup-plugin-dts, npm-dts)](https://github.com/timocov/dts-bundle-generator/discussions/68)
- [Use-cases of incorrect result for dts-bundle-generator](https://github.com/Swatinem/rollup-plugin-dts/issues/5)

## License

MIT License

Copyright © 2022 "Ron Don Jon" Daniel Kastenholz.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## History

| Version | Changes                                                                                           |
| ------- | ------------------------------------------------------------------------------------------------- |
| 1.0.0   | Initial version                                                                                   |
| 1.1.0   | Support rollup configs with output dir instead of an output filename; add usage info to README.md |
| 1.1.1   | Fix `path.join()`                                                                                 |
| 1.2.0   | Improve README.md                                                                                 |
| 1.3.0   | Improve README.md                                                                                 |
| 1.3.1   | fix repository link in `package.json`                                                             |
| 1.4.0   | Update dependencies                                                                               |
